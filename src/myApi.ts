/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface GitFlightfactorAeroXUpdaterBackendInternalDomainsMessage {
  /** Error message */
  message?: string;
}

export interface GitFlightfactorAeroXUpdaterBackendPkgDomainModelConsumer {
  /** Block message, null - means the consumer is not blocked otherwise it is blocked with the message. */
  block_msg?: GithubComVolatiletechNullV8String;
  created?: string;
  id?: number;
  modified?: string;
  /** Any text note visible only for admins. */
  notes?: GithubComVolatiletechNullV8String;
}

export interface GitFlightfactorAeroXUpdaterBackendPkgDomainModelProduct {
  /** Block message, null - means the product is not blocked otherwise it is blocked with the message. Is used to manage product distribution: start/pause/stop */
  block_msg?: GithubComVolatiletechNullV8String;
  created?: string;
  /** Directory name where a product is stored on the server. Relative to the distributor's one. */
  dir_name?: string;
  id?: number;
  /** A product name that is used inside the system. */
  local_name?: string;
  modified?: string;
  /** Any text note. */
  notes?: GithubComVolatiletechNullV8String;
  /** Id of the project a product belongs to. */
  project_id?: number;
  /** Not 0 if a product has public beta access. */
  public_beta?: GithubComVolatiletechNullV8Uint8;
  /** This is 16 bytes of uuid-V7. Represents public unique id of a product. Can be used in various sceneries to identifies the product. */
  public_id?: string;
  /** It is the id from the old database and the old xupdater version and used for backward compatibility. Some day it should be dropped (when all consume clients are able to work with new variant). */
  public_id_legacy?: GithubComVolatiletechNullV8String;
  /** A product name that is read from a store database. I am not sure if it should be unique, so it is normal if we turn uniqueness off in future. */
  store_name?: GithubComVolatiletechNullV8String;
  /** Id of a store product this product is associated to. */
  store_product_id?: GithubComVolatiletechNullV8Uint;
}

export interface GitFlightfactorAeroXUpdaterBackendPkgDomainModelProject {
  created?: string;
  /** Id of the distributor a product belongs to. */
  distributor_id?: number;
  id?: number;
  modified?: string;
  project?: string;
}

export interface GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelConsumer {
  /** Block message, null - means the consumer is not blocked otherwise it is blocked with the message. */
  block_msg?: GithubComVolatiletechNullV8String;
  created?: string;
  id?: number;
  modified?: string;
  /** Any text note visible only for admins. */
  notes?: GithubComVolatiletechNullV8String;
}

export interface GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelInsertComplexConsumer {
  /** @example 0 */
  is_tester?: number;
  /** @example "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1" */
  key_hash?: string;
  notes?: GithubComVolatiletechNullV8String;
  /** OrderNum   int         `boil:"order_num" json:"order_num" toml:"order_num" yaml:"order_num" example:"1100"` */
  order_num?: GithubComVolatiletechNullV8Uint;
  /** @example 2001 */
  product_id?: number;
  /** @example 1 */
  provider_id?: number;
  /**
   * New user name
   * @example "testinsert@test.com"
   */
  user_name?: string;
}

export interface GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelProductCustome {
  block_msg_licensekey?: GithubComVolatiletechNullV8String;
  block_msg_licensekey_transition?: GithubComVolatiletechNullV8String;
  /** Block message, null - means the product is not blocked otherwise it is blocked with the message. Is used to manage product distribution: start/pause/stop */
  block_msg_product?: GithubComVolatiletechNullV8String;
  created_licensekey?: string;
  created_licensekey_transition?: string;
  created_product?: string;
  /** Directory name where a product is stored on the server. Relative to the distributor's one. */
  dir_name?: string;
  final_product?: boolean;
  id?: number;
  /** For license */
  id_licensekey?: number;
  /** For licensekeytransition */
  id_licensekey_transition?: number;
  is_tester_licensekey?: number;
  key_hash?: string;
  /** A product name that is used inside the system. */
  local_name?: string;
  modified_licensekey?: string;
  modified_licensekey_transition?: string;
  modified_product?: string;
  notes_licensekey?: GithubComVolatiletechNullV8String;
  notes_licensekey_transition?: GithubComVolatiletechNullV8String;
  /** Any text note. */
  notes_product?: GithubComVolatiletechNullV8String;
  order_num_licensekey?: GithubComVolatiletechNullV8Uint;
  order_num_licensekey_transition?: GithubComVolatiletechNullV8Uint;
  /** Id of the project a product belongs to. */
  project_id?: number;
  provider_licensekey?: string;
  provider_licensekey_transition?: string;
  /** Not 0 if a product has public beta access. */
  public_beta?: GithubComVolatiletechNullV8Uint8;
  /** This is 16 bytes of uuid-V7. Represents public unique id of a product. Can be used in various sceneries to identifies the product. */
  public_id?: string;
  /** It is the id from the old database and the old xupdater version and used for backward compatibility. Some day it should be dropped (when all consume clients are able to work with new variant). */
  public_id_legacy?: GithubComVolatiletechNullV8String;
  /** A product name that is read from a store database. I am not sure if it should be unique, so it is normal if we turn uniqueness off in future. */
  store_name?: GithubComVolatiletechNullV8String;
  /** Id of a store product this product is associated to. */
  store_product_id?: GithubComVolatiletechNullV8Uint;
}

export interface GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelProductCustomeSlice {
  block_msg?: GithubComVolatiletechNullV8String;
  /** Block message, null - means the consumer is not blocked otherwise it is blocked with the message. */
  block_msg_consumer?: GithubComVolatiletechNullV8String;
  block_msg_licensekey?: GithubComVolatiletechNullString;
  block_msg_licensekey_transition?: GithubComVolatiletechNullString;
  consumer_id?: number;
  created?: string;
  created_consumer?: string;
  created_licensekey?: string;
  created_licensekey_transition?: string;
  deactivation_msg?: GithubComVolatiletechNullV8String;
  id?: number;
  id_consumer?: number;
  id_transitionid_transition?: number;
  /** 0 - no access to test packages, 1 - access to beta, 2 - access to alpha. */
  is_tester?: number;
  /**
   * TDB: We need to select an algorithm and set the appropriate type. Argon? SSH256, SSH512? + Salt
   * About unique index: If real life demand another behavior, consider to combine it with other fields like username or product_id firstly.
   */
  key_hash?: string;
  /** The previous used hash (MD5) must be replaces by new. */
  key_hash_legacy?: GithubComVolatiletechNullString;
  main_notes?: GithubComVolatiletechNullV8String;
  /** Any text note visible only for admins. */
  main_notes_consumer?: GithubComVolatiletechNullV8String;
  modified?: string;
  modified_consumer?: string;
  modified_licensekey?: string;
  modified_licensekey_transition?: string;
  notes?: GithubComVolatiletechNullV8String;
  notes_consumer?: GithubComVolatiletechNullV8String;
  notes_licensekey?: GithubComVolatiletechNullString;
  notes_licensekey_transition?: GithubComVolatiletechNullString;
  /** Order id a key was created from. */
  order_num?: GithubComVolatiletechNullUint;
  /** Order id a key was created from. */
  order_num_transition?: GithubComVolatiletechNullUint;
  productCustomeSlice?: GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelProductCustome[];
  product_id_licensekey?: number;
  product_transition_id?: number;
  /** Key provider, for example: Store1, Store2, manually etc... */
  provider_id_licensekey?: number;
  provider_id_transition?: number;
  user_name?: string;
  /** Who a key was inserted for. */
  user_name_id?: number;
  user_name_id_transition?: number;
  username_id?: number;
}

export interface GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelSeachComplex {
  /** @example "" */
  key_hash?: string;
  /** @example "mrosoni0@psu.edu" */
  user_name?: string;
}

export interface GithubComVolatiletechNullString {
  string?: string;
  valid?: boolean;
}

export interface GithubComVolatiletechNullUint {
  uint?: number;
  valid?: boolean;
}

export interface GithubComVolatiletechNullV8String {
  string?: string;
  valid?: boolean;
}

export interface GithubComVolatiletechNullV8Uint {
  uint?: number;
  valid?: boolean;
}

export interface GithubComVolatiletechNullV8Uint8 {
  uint8?: number;
  valid?: boolean;
}

export type HttpHeader = Record<string, string[]>;

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
  Text = "text/plain",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = "https://3be9-91-150-13-86.ngrok-free.app/v1";
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === "number" ? value : `${value}`)}`;
  }

  protected addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  protected addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join("&");
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
    return keys
      .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
    [ContentType.Text]: (input: any) => (input !== null && typeof input !== "string" ? JSON.stringify(input) : input),
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === "object" && property !== null
            ? JSON.stringify(property)
            : `${property}`,
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  protected mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  protected createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
      ...requestParams,
      headers: {
        ...(requestParams.headers || {}),
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
      },
      signal: cancelToken ? this.createAbortSignal(cancelToken) : requestParams.signal,
      body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title API
 * @version 1.0
 * @license Apache 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 * @termsOfService http://swagger.io/terms/
 * @baseUrl https://3be9-91-150-13-86.ngrok-free.app/v1
 * @contact API Support <support@lightfactor.aero> (https://flightfactor.aero/)
 *
 * This is a API server.
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  consumers = {
    /**
     * @description Find all consumers.
     *
     * @tags v1/consumers
     * @name ConsumersList
     * @summary Find all consumers.
     * @request GET:/consumers
     */
    consumersList: (params: RequestParams = {}) =>
      this.request<GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelConsumer, any>({
        path: `/consumers`,
        method: "GET",
        format: "json",
        ...params,
      }),

    /**
     * @description Insert new consumer only in one table use insted InserConsumerComplex.
     *
     * @tags v1/consumers
     * @name InsertNewConsumer
     * @summary Insert new consumer only in one table! Use insted /consumers/insert.
     * @request POST:/consumers
     */
    insertNewConsumer: (data: GitFlightfactorAeroXUpdaterBackendPkgDomainModelConsumer, params: RequestParams = {}) =>
      this.request<HttpHeader, GitFlightfactorAeroXUpdaterBackendInternalDomainsMessage>({
        path: `/consumers`,
        method: "POST",
        body: data,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description Insert new consumer complite.
     *
     * @tags v1/consumers
     * @name InserConsumerComplex
     * @summary Insert new consumer complite.
     * @request POST:/consumers/insert
     */
    inserConsumerComplex: (
      data: GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelInsertComplexConsumer,
      params: RequestParams = {},
    ) =>
      this.request<HttpHeader, GitFlightfactorAeroXUpdaterBackendInternalDomainsMessage>({
        path: `/consumers/insert`,
        method: "POST",
        body: data,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description Complex find consumer by email or key.
     *
     * @tags v1/consumers
     * @name SelectConsumerComplex
     * @summary Comlex find consumer by email or key.
     * @request POST:/consumers/search
     */
    selectConsumerComplex: (
      data: GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelSeachComplex,
      params: RequestParams = {},
    ) =>
      this.request<
        GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelProductCustomeSlice,
        GitFlightfactorAeroXUpdaterBackendInternalDomainsMessage
      >({
        path: `/consumers/search`,
        method: "POST",
        body: data,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description Find consumer by ID.
     *
     * @tags v1/consumers
     * @name ConsumersDetail
     * @summary Find consumer by ID.
     * @request GET:/consumers/{id}
     */
    consumersDetail: (id: number, params: RequestParams = {}) =>
      this.request<GitFlightfactorAeroXUpdaterBackendPkgUsecaseModelConsumer, any>({
        path: `/consumers/${id}`,
        method: "GET",
        format: "json",
        ...params,
      }),
  };
  healthCheck = {
    /**
     * @description get the status of server.
     *
     * @tags root
     * @name HealthCheckList
     * @summary Show the status of server.
     * @request GET:/health_check
     */
    healthCheckList: (params: RequestParams = {}) =>
      this.request<HttpHeader, any>({
        path: `/health_check`,
        method: "GET",
        format: "json",
        ...params,
      }),
  };
  products = {
    /**
     * @description Find all products.
     *
     * @tags v1/products
     * @name FindAllProducts
     * @summary Find all products.
     * @request GET:/products
     */
    findAllProducts: (params: RequestParams = {}) =>
      this.request<GitFlightfactorAeroXUpdaterBackendPkgDomainModelProduct[], any>({
        path: `/products`,
        method: "GET",
        format: "json",
        ...params,
      }),

    /**
     * @description Find one product by ID.
     *
     * @tags v1/products
     * @name FindProductById
     * @summary Find product by ID.
     * @request GET:/products/{id}
     */
    findProductById: (id: number, params: RequestParams = {}) =>
      this.request<GitFlightfactorAeroXUpdaterBackendPkgDomainModelProduct[], any>({
        path: `/products/${id}`,
        method: "GET",
        format: "json",
        ...params,
      }),
  };
  projects = {
    /**
     * @description Find all projects.
     *
     * @tags v1/projects
     * @name FindAllProjects
     * @summary Find all projects.
     * @request GET:/projects
     */
    findAllProjects: (params: RequestParams = {}) =>
      this.request<
        GitFlightfactorAeroXUpdaterBackendPkgDomainModelProject[],
        GitFlightfactorAeroXUpdaterBackendInternalDomainsMessage
      >({
        path: `/projects`,
        method: "GET",
        format: "json",
        ...params,
      }),

    /**
     * @description Insert new project.
     *
     * @tags v1/projects
     * @name InsertProject
     * @summary Insert new project.
     * @request POST:/projects
     */
    insertProject: (data: GitFlightfactorAeroXUpdaterBackendPkgDomainModelProject, params: RequestParams = {}) =>
      this.request<HttpHeader, any>({
        path: `/projects`,
        method: "POST",
        body: data,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description Find project by ID.
     *
     * @tags v1/projects
     * @name FindProjectById
     * @summary Find project by ID.
     * @request GET:/projects/{id}
     */
    findProjectById: (id: number, params: RequestParams = {}) =>
      this.request<GitFlightfactorAeroXUpdaterBackendPkgDomainModelProject, any>({
        path: `/projects/${id}`,
        method: "GET",
        format: "json",
        ...params,
      }),

    /**
     * @description Update project.
     *
     * @tags v1/projects
     * @name UpdateProject
     * @summary Update project.
     * @request PUT:/projects/{id}
     */
    updateProject: (
      id: number,
      data: GitFlightfactorAeroXUpdaterBackendPkgDomainModelProject,
      params: RequestParams = {},
    ) =>
      this.request<HttpHeader, any>({
        path: `/projects/${id}`,
        method: "PUT",
        body: data,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description Delete project by ID.
     *
     * @tags v1/projects
     * @name DeleteProjectById
     * @summary Delete project by ID.
     * @request DELETE:/projects/{id}
     */
    deleteProjectById: (id: number, params: RequestParams = {}) =>
      this.request<GitFlightfactorAeroXUpdaterBackendPkgDomainModelProject, any>({
        path: `/projects/${id}`,
        method: "DELETE",
        format: "json",
        ...params,
      }),
  };
}
